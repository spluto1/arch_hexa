---
theme: dracula
background: /andrew-draper-Q7_5VOi_xew-unsplash.jpg
class: text-center
highlighter: shiki
lineNumbers: false
info: |
  ## Basic intro to Hexagonal Architecture.

  Created with [Sli.dev](https://sli.dev)
drawings:
  persist: false
transition: slide-left
title: Hexagonal Architecture
layout: cover
---
# Hexagonal Architecture

Isolate dependencies properly from your business code

<div class="abs-br m-6 flex gap-2">
  <button @click="$slidev.nav.openInEditor()" title="Open in Editor" class="text-xl slidev-icon-btn opacity-50 !border-none !hover:text-white">
    <carbon:edit />
  </button>
  <a href="https://gitlab.com/javanarior/arch_hexa" target="_blank" alt="GitLab"
    class="text-xl slidev-icon-btn opacity-50 !border-none !hover:text-white">
    <carbon-logo-gitlab />
  </a>
</div>

<footer class="absolute bottom-0 left-0 p-2 ">
  <a class="foot-note-color" href="https://unsplash.com/@andalexander">Photo: Andrew Draper</a>
</footer>

<!--
Why have we dealt with hexagonal architecure?
-->

---
title: Agenda
transition: fade-out
level: 2
---

# Agenda

1. History
1. Overview
1. Primary Ports
1. Secondary Ports
1. Where to go from here
1. Take away
1. Q & A
1. References

<!--
The naming is a little bit interesting
Big picture of hexagonal architecture before I talk about the elements primary and secondary port
hexagonal architecture is not an all-in approach, the missing part is here
What should you take away, q&a session

-->

---
layout: section
level: 1
---

# History

<!--
Let's talk about origin and naming
-->

---
title: History
transition: fade-out
level: 2
---

# History

* First described by Alistar Cockburn in 2005
* First published name was ports and adapters
* A unique and easy to draw graphical represention should be used => hexagon
* Therefore, the name hexagonal-architecture 

<!--
-->

---
title: Overview
layout: section
level: 1
---

# Overview

<!--
Let's take a look on hexagonal architecture from a bird perspective
-->

---
title: Overview
layout: center
level: 2
---

# Overview

![Remote Image](https://www.happycoders.eu/wp-content/uploads/2023/01/hexagonal-architecture.v2-600x431.png)

<footer class="absolute bottom-0 left-0 p-2 ">
  <a class="foot-note-color" href="https://www.happycoders.eu/software-craftsmanship/hexagonal-architecture/">Picture: Sven Woltmann, https://www.happycoders.eu/software-craftsmanship/hexagonal-architecture/</a>
</footer>

<!--
In the center is our application, the business code. Our Application is quite isolated regarding
the external elements. Only through the ports our application interacts with the rest of the world.
When we draw a line in the middle of the application top/town, we see on the left hand side 
the primary ports. Through the primary ports our application get external requests, ui,
rest, messaging, etc. On the other, the right hand side, we see the secondary ports. 
These are our dependencies to other external systems like DB, other Services, etc.

One other thing to note, the hexagonal architecture makes no guidelines how we
structure our application inside. This is completly up to you.
-->

---
title: Dependencies
layout: center
transition: fade-out
level: 2
---

# Dependencies

![Remote Image](https://www.happycoders.eu/wp-content/uploads/2023/01/hexagonal-architecture-dependency-rule.v2-800x458.png)

<footer class="absolute bottom-0 left-0 p-2 ">
  <a class="foot-note-color" href="https://www.happycoders.eu/software-craftsmanship/hexagonal-architecture/">Picture: Sven Woltmann, https://www.happycoders.eu/software-craftsmanship/hexagonal-architecture/</a>
</footer>

<!--

-->

---
title: Primary Port
layout: section
level: 1
---
# Primary Port

<!--
-->

---
title: Primary Port
layout: default
transition: fade-out
level: 2
---

# Primary Port

<!--
-->

```plantuml
@startuml
package "application" {
    interface Port <<Primary Port>> {
        coolUsecase
    }
    class Application implements Port {
        coolUsecase
    }
}

package "adapter" {
    class MockAdapter 
    class UiAdapter
    class RestAdapter
}

Port <-- MockAdapter
Port <-- UiAdapter 
Port <-- RestAdapter

@enduml
```

<!--

-->

---
title: Secondary Port
layout: section
level: 1
---
# Secondary Port

<!--
-->

---
title: # Secondary Port
layout: default
transition: fade-out
level: 2
---

# Secondary Port

```plantuml {scale: 1}
@startuml
package "application" {
    interface PersistencePort <<Secondary Port>> {
      businessOrientedPersistenceCall
    }
    class Application
}

package "adapter.persistence.memory" {
    class InMemoryRepository implements application.PersistencePort {
      businessOrientedPersistenceCall
    } 
}
package "adapter.persistence.jpa" {
    class JpaRepository implements application.PersistencePort {
      businessOrientedPersistenceCall
    }
}
package "adapter.persistence.reactive" {    
    class ReactiveJpaRepository implements application.PersistencePort {
      businessOrientedPersistenceCall
    }
}

PersistencePort <-- Application : uses

@enduml
```

<!--
 
-->

---
title: Lessons Learned
layout: section
level: 1
---
# Lessons Learned

<!--

-->


---
title: Lessons Learned II
layout: statement
level: 1
---
# Structure your code in the Adapter properly

<!--
Don't start to map in the Application code.
Don't introduce additional mapping.
Don't put everything in the Adapter.
It's fine to use a client
-->

---
title: Lessons Learned III
layout: center
transition: fade-out
level: 1
---

# Use ![Remote Image](https://www.archunit.org/assets/ArchUnit-Logo.png)

<!--
Use ArchUnit!
-->

---
title: Where to go from here
layout: section
level: 1
---
# Where to go from here

<!--

-->

---
title: Where to go from here
transition: fade-out
level: 1
---
# Where to go from here

- Domain Driven Design
- Clean Architecture
- Onion Architecture
- CQRS
- SAGA

<!--
CQRS - Command and Query Responsibility Segregation
SAGA - Distribution Transactions
-->

---
title: Takeaway
layout: section
level: 1
---

# Takeaway

<!--
 
-->
---
title: Takeaway
transition: fade-out
level: 1
---

# Takeaway

- Use a Hexagonal Architecture to isolate dependencies, also to internal systems

- The Hexagonal Architecture uses Dependency Inversion, so that the Dependencies **ALWAYS** 
  goes from the outer side to the inner side

- The Hexagonal Architecture does not make any guidelines for your business code, that's up to you

- Don't place everything in your Adapter

- Use ArchUnit, even if you don't use Hexagonal Architecture

<!--
 
-->

---
title: Q&A
layout: section
transition: fade-out
level: 1
---
# Q & A


<!--
One Point that I didn't covered in the slides is Pro & Cons or when to use and when not.

-->
---
title: References
transition: fade-out
level: 1
---

<!--
-->
# References

Sven Woltmann: Hexagonal Architecture - What Is It? Why Should You Use It?
https://www.happycoders.eu/software-craftsmanship/hexagonal-architecture/

Alistair Cockburn: Hexagonal architecture<br>
https://alistair.cockburn.us/hexagonal-architecture/

Herberto Graça: Ports & Adapters Architecture[^1]<br>
https://herbertograca.com/2017/09/14/ports-adapters-architecture/

Robert C. Martin (Uncle Bob): Clean Architecture<br>
https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html

Jeffrey Palermo: Onion Architecture<br>
https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/

Herberto Graça: DDD, Hexagonal, Onion, Clean, CQRS, … How I put it all together
https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/

[^1]: I found the Term "Ports & Adapters Architecture" first on this page, in my opinion it should be "ports and adapters (pattern)" or "Hexagonal architecture"  

---
title: That's all Folks
layout: end
---
