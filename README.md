# Introduction in Hexagonal Architecture

To start the slide show:

- `npm install`
- `npm run dev`
- public http://localhost:3030
- presenter http://localhost:3030/presenter/1

The Slides also available at https://javanarior.gitlab.io/arch_hexa

Learn more about Slidev on [documentations](https://sli.dev/).
